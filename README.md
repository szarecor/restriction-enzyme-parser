# README #

In-progress code written primarily by Chris Lawrence. 

The code is intended to take restriction enzyme data from rebase in flat file form and parse it into a simple relational database.

### How do I get set up? ###

It requires sqlite3 and sqlalchemy