f = open ('rebase.txt','r')
lines = f.readlines()
lines = lines[37:]
Names = []
Sequence = []
Isozimes = []
Methylationsite = []
Source = []
Reference = []
for l in lines:

	if '<1>' in l: # equals/designates what is in front of line
		e = {}
		e['name'] = l[3:].strip('\n')
	elif l == '\n':
		Names.append(e) #names.append(e) sends to the list
		e = {}
	if '<3>' in l:
		f = {}
		f['sequence'] = l[3:].strip('\n')
	elif l == '\n':
		Sequence.append(f)
		f = {}
	if '<2>' in l:
		g = {}
		g['Isozimes'] = l[3:].strip('\n')
	elif l == '\n':
		Isozimes.append(g)
		g = {}
	if '<4>' in l:
		h = {}
		h['Methylationsite'] = l[3:].strip('\n')
	elif l == '\n':
		Methylationsite.append(h)
		h = {}
	if '<5>' in l:
		j = {}
		j['Source'] = l[3:].strip('\n')
	elif l == '\n':
		Source.append(j)
		j = {}
	if '<6>' in l:
		k = {}
		k['reference'] = l[3:].strip('\n')
	elif l == '\n':
		Reference.append(k)
		k = {}


import sys

print Names
print '-' * 100
print Sequence
print '-' * 100
print Isozimes
print '-' * 100

sys.exit(0)



from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from sqlalchemy_declarative import ENZYMES, Base
engine = create_engine('sqlite:///Renzymes_example.db')
# declaratives can be accessed through a DBSession instance
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
session = DBSession()


# Insert the list in the ENZYMES table
new_Enzymes = ENZYMES( name = Names, sequence = Sequence, isozimes = Isozimes, methylationsite = Methylationsite, commercialsource = Source, reference = Reference)
session.add(new_Enzymes)
session.commit()
