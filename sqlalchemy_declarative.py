import os
import sys
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine
 
Base = declarative_base()
class ENZYMES(Base):
    __tablename__ = "ENZYMES"
    # define columns for the table
    id = Column(Integer, primary_key=True)
    name = Column(String(50), nullable=False)
    sequence = Column(String(250), nullable=False)
    isozimes = Column(String(250), nullable=False)
    methylationsite = Column(String(250), nullable=False)
    commercialsource = Column(String(250), nullable=False)
    reference = Column(String(250), nullable=False)
     
# stores data
engine = create_engine('sqlite:///RENZYMES_example.db')
 
# Create all tables in the engine. 
Base.metadata.create_all(engine)